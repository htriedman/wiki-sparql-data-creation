# imports
from rdflib.plugins.sparql.parser import parseQuery
import mwapi
from mwapi.errors import APIError
import mwparserfromhell as parser
import re
import os
import pandas as pd
import logging
from datetime import date

# constants
templates = [
    "Wikidata list",
    "SPARQL",
    "SPARQL2",
    "SPARQL5",
    "SPARQL Inline",
    "Wdquery",
    "Complex constraint"
]

os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'

template_regex_string = "|".join([f"{{{{\s*[{t[0].lower()}|{t[0].upper()}]{t[1:]}\s*\|" for t in templates])

wikis = set()
with open('wikis.txt', 'r') as f:
    for line in f:
        # wikis.add(f'https://{line[:-1]}')
        wikis.add(line[:-1])

session = mwapi.Session('https://mw-api-int-ro.discovery.wmnet:4446', user_agent="htriedman sparql corpus bot")

logger = logging.getLogger(__name__)
logging.basicConfig(filename=f'template_fixes_{date.today().strftime("%Y_%m_%d")}.log', level=logging.DEBUG)

# helper functions
def is_sparql_query_valid(query):
    try:
        # Attempt to prepare a SPARQL query. This will parse the query.
        parseQuery(query)
        return True  # If parsing succeeds, the query is valid.
    except:
        return False  # If parsing fails, the query is invalid.
    
def get_transcluded_pages(session, template):
    continued = session.get(
        formatversion=2,
        action='query',
        prop='transcludedin',
        titles=f"Template:{template}",
        continuation=True
    )

    pages = []
    try:
        for portion in continued:
            if 'query' in portion:
                for page in portion['query']['pages']:
                    try:
                        for transcluded in page['transcludedin']:
                            pages.append(transcluded["title"])
                    except:
                        pass
            else:
                logger.error("MediaWiki returned empty result batch.")
    except APIError as error:
        raise ValueError(
            "MediaWiki returned an error:", str(error)
        )
    
    return pages

def extract_sparql(session, p, t):
    resp = session.get(
        formatversion=2,
        action='query',
        prop='revisions',
        rvslots='*',
        rvprop='content',
        titles=p
    )

    content = resp['query']['pages'][0]['revisions'][0]['slots']['main']['content']
    wikitext = parser.parse(content)
    templates = wikitext.filter_templates()
    templates = list(filter(lambda template: t in template, templates))
    if t == "Wikidata list":
        templates = list(filter(lambda template: template != "{{Wikidata list end}}", templates))
    
    out = []
    for template in templates:
        out.append(template.split("|")[1].split("=")[1])
        
    return out

def check_templates(template):
    for t in templates:
        if t in template:
            return True
    return False

def split_string_and_extract_preceding(s, delimiter):
    parts = s.split(delimiter)  # Split the string by the delimiter.
    preceding_texts = []  # Initialize a list to hold the preceding text segments.
    
    search_pos = 0  # Start position for each search iteration.
    for part in parts[:-1]:  # Ignore the last part since no split occurs after it.
        # Calculate the start position of the current part in the original string.
        current_part_start = s.find(part, search_pos)
        # Calculate the end position of the current part, which is the split point.
        split_point = current_part_start + len(part)
        
        # Determine the start position for extracting preceding characters.
        # It's the greater of 0 and split_point - 300 to avoid negative indices.
        extract_start = max(0, split_point - 300)
        
        # Extract up to 250 characters preceding the split point.
        preceding_text = s[extract_start:split_point]
        preceding_texts.append(preceding_text)
        
        # Update the search position for the next iteration.
        search_pos = split_point + len(delimiter)
    
    return preceding_texts[0]

def get_sparql_and_surrounding(title, session):
    out = []
    resp = session.get(
        formatversion=2,
        action='query',
        prop='revisions',
        rvslots='*',
        rvprop='content',
        titles=title
    )
    content = resp['query']['pages'][0]['revisions'][0]['slots']['main']['content']
    wikitext = parser.parse(content)
    wikitext_templates = list(filter(check_templates, wikitext.filter_templates()))
    wikitext_templates = list(filter(lambda template: template != "{{Wikidata list end}}", wikitext_templates))
    wikitext_templates = list(filter(lambda template: template != "{{Wikidata list header}}", wikitext_templates))
    wikitext_templates = list(filter(lambda template: template != "{{Wikidata list menu}}", wikitext_templates))
    wikitext_templates = list(filter(lambda template: template != "{{Wikidata list documentation}}", wikitext_templates))
    if '{{query page' in wikitext:
        print("query page")
        lede = wikitext[:250]
        query = re.split("query\s*=\s*", str(wikitext))[1].split("|")[0]
        text = None
        results = None

        out.append({"title": title, "lede": lede, 'preceding_text': text, 'query': query, 'results': results})
    
    elif len(wikitext_templates) > 0:
        for wt in wikitext_templates:
            lede = wikitext[:250]
            text = split_string_and_extract_preceding(wikitext, str(wt))
            results = None
            if "wdquery" in wt.lower():
                query = re.split(r"query\s*=\s*", str(wt))[1].split("|")[0]
            elif "complex constraint" in wt.lower():
                lede = re.split(r"label\s*=\s*", str(wt))[1].split("|")[0]
                text = re.split(r"description\s*=\s*", str(wt))[1].split("|")[0]
                query = re.split(r"sparql\s*=\s*", str(wt))[1].split("|")[0]
            elif "wikidata list" in wt.lower():
                ts = wikitext.find(str(wt))
                te = wikitext.lower().find("{{wikidata list end}}")
                truncated = wikitext[ts:te]
                results = truncated[truncated.find("{|"):truncated.find("|}")]
                valid = False
                i = 0
                possible_splits = [r"\|section", r"\|", r"\s+\|"]
                while not valid:
                    query = re.split(possible_splits[i], re.split(r"sparql\s*=\s*", str(wt), maxsplit=1)[1])[0]#.split("|")[0]
                    valid = is_sparql_query_valid(query)
                    i += 1
                    if i >= len(possible_splits):
                        break
            elif "doc example" in wt.lower():
                query = re.split(r"content=\s*<pre>\s*{{SPARQL\s*|\s*query=", str(wt))[1]
            elif "sparql label" in wt.lower():
                continue
            else:
                query = wt.split("|")[1].split("=", 1)[1]
                if not is_sparql_query_valid(query):
                    query = re.split(r"query\s*=\s*", str(wt), maxsplit=1)[1]
                
            
            if query.endswith("\n}}"):
                query = query[:-3]
            if query.endswith("}}"):
                query = query[:-2]
            query = query.replace("{{!}}", "|")
            if not is_sparql_query_valid(query):
                print(f'invalid query: {query}')

            out.append({"title": title, "lede": lede, 'preceding_text': text, 'query': query, 'results': results})
        return out
    return None

# main function
def main():
    df = pd.DataFrame(columns=['project', 'title', 'lede', 'preceding_text', 'query', 'results'])
    for w in wikis:
        fail_ctr = 0
        logger.info(w)
        session.headers['Host'] = w
        all_pages = set()
        for t in templates:
            pages = get_transcluded_pages(session, t)
            logger.info(f'template {t} occurs {len(pages)} times on {w}')
            all_pages.update(pages)
        logger.info(f'there are a total of {len(all_pages)} sparql-related pages on {w}')  
        for i, p in enumerate(all_pages):
            # time.sleep(0.25)
            if i % 500 == 0:
                logger.info(f'templates seen: {i}')
            # out = get_sparql_and_surrounding(p, session)
            # if out is None:
            #     logger.info("none")
            #     continue
            # for i in out:
            #     i['project'] = w
            #     logger.info(i)
            # df = pd.concat([df, pd.DataFrame.from_dict(out)])
            try:
                out = get_sparql_and_surrounding(p, session)
                if out is None:
                    continue
                for i in out:
                    i['project'] = w
                df = pd.concat([df, pd.DataFrame.from_dict(out)])
            except Exception as error:
                # handle the exception
                print(f"An exception occurred: {type(error).__name__}")
                print(error)

                fail_ctr += 1
                if fail_ctr % 50 == 0 and fail_ctr != 0:
                    logger.info(f'failures: {fail_ctr}')
                continue

    df['validity'] = df['query'].map(is_sparql_query_valid)
    df.to_pickle('wikidata-sparql-templates-bug-fixes.pkl')
    
if __name__ == "__main__":
    main()
